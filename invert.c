#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include "getopt.h"
#include "mpi_util.h"
#include "mpi/mpi.h"

int main(int argc, char* argv[]) {
	init(&argc, &argv) ;

	int i,j ;
	int dimension ;
	int choice ;
	// Handle command line arguments. //
	while (1) {
		choice = getopt(argc, argv, "n:") ;
		if (choice == -1) {
			break ;
		}
		switch(choice) {
			case 'n':
				dimension = atoi(optarg) ;
				break;
			default:
				return EXIT_FAILURE;
		}
	}

	if (argc != 3) {
		fprintf(stderr, "Insufficient Arguments\n") ;
		return EXIT_FAILURE ;
	}

	Matrix * orig_matrix = alloc_matrix(dimension) ;

	for (i = 0 ; i < dimension ; ++i) {
		for (j = 0 ; j < dimension ; ++j) {
			set_matrix_entry(orig_matrix, i, j, rand()%100+1) ;
		}
	}
	// Print the original matrix. //
	if (rank == 0) {
		printf(".........ORIGINAL..........\n");
	}
	MPI_Barrier(MPI_COMM_WORLD) ;
	print_matrix(orig_matrix) ;
	Matrix * invert_matrix = invert(orig_matrix) ;
	// Print the inverse of the matrix. //
	if (rank == 0) {
		printf(".........INVERT..........\n");
	}
	MPI_Barrier(MPI_COMM_WORLD) ;
	print_matrix(invert_matrix) ;
	
	free_matrix(orig_matrix) ;
	free_matrix(invert_matrix) ;
	MPI_Finalize();

	return EXIT_SUCCESS ;
}
