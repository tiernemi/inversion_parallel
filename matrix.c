#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include "mpi_util.h"
#include "mpi/mpi.h"
#include <string.h>
#include <math.h>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  get_c_host
 *    Arguments:  Matrix * mat - The matrix where the data is stored.
 *                int j - The column index.
 *      Returns:  The rank of the process storing the column.
 *  Description:  This function tells us which process is holding the column.
 * =====================================================================================
 */

static int get_col_host(Matrix * mat, int col_index) {
  	return col_index % nproc ;	
}		/* -----  end of function get_c_host  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  get_local_c_index
 *    Arguments:  Matrix * mat - The matrix where the data is stored.
 *                int col_index - The column index.
 *      Returns:  The local index of the column on this process. -1 if it doesnt have one.
 *  Description:  Converts a global column index to a local index on the process.
 * =====================================================================================
 */

static int get_local_col_index(Matrix * m, int col_index) {
	if ((col_index % nproc)==rank) {
		return (col_index-rank) / nproc ;
	}
	return -1; 
}		/* -----  end of function get_local_c_index  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  mat_localarray_index
 *    Arguments:  Matrix * m - The matrix where the data is stored.
 *                int i - Local row index.
 *                int j - Local column index.
 *      Returns:  The local 1-D array index for a given i,j index pair.
 *  Description:  Converts an i,j pair to a local 1-D index.
 * =====================================================================================
 */

static int mat_localarray_index(Matrix * mat, int i, int j) {
	return j + mat->block_dimension * i; 	
}		/* -----  end of function  mat_localarray_index  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  alloc_matrix
 *    Arguments:  int dimension - The dimension n of the nxn matrix.
 *      Returns:  Pointer to heap allocated Matrix struct.
 *  Description:  Initialises Matrix struct across all processes. Must be called in
 *                a parallel regime.
 * =====================================================================================
 */

Matrix * alloc_matrix(int dimension) {
	Matrix * new_matrix = malloc(sizeof(Matrix)) ;
	new_matrix->dimension = dimension ;
	int remainder, tot_block_size ;

	// Calculate dimensions per process and account for remainder data. //
	remainder = dimension % nproc ;
	if(remainder == 0) {
		new_matrix->block_dimension = dimension/nproc ;
	} else {
		new_matrix->block_dimension = dimension/nproc ;
		if (rank < remainder) new_matrix->block_dimension++ ;
	}

	tot_block_size = new_matrix->dimension * new_matrix->block_dimension ;
	new_matrix->data = calloc(tot_block_size, sizeof(double)) ;

	return new_matrix ;
}		/* -----  end of function alloc_matrix  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  free_matrix
 *    Arguments:  Matrix * mat - Matrix to be freed from the heap.
 *  Description:  Frees dynamically allocated Matrix.
 * =====================================================================================
 */

void free_matrix(Matrix * mat) {
	free(mat->data) ;
	free(mat) ;
}		/* -----  end of function free_matrix  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  set_matrix_entry
 *    Arguments:  Matrix * m - Matrix containing data to be overwritten.
 *                int i - True row index.
 *                int j - True column index.
 *  Description:  Sets the value located at the global i,j index of the true Matrix.
 * =====================================================================================
 */

void set_matrix_entry(Matrix * mat, int i, int j, double value) {
	int local_j = get_local_col_index(mat,j);

	if (local_j > -1) {
    	mat->data[mat_localarray_index(mat,i,local_j)] = value ;
	}
}		/* -----  end of function set_matrix_entry  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  get_matrix_entry
 *    Arguments:  Matrix * m - Matrix containing the data.
 *                int i - True row index.
 *                int j - True column index.
 *      Returns:  The value at i,j.
 *  Description:  Pulls the value from the process containing it and sends to all processes.
 * =====================================================================================
 */

double get_matrix_entry(Matrix * mat, int i, int j) {
	double value ;
	int root_process = get_col_host(mat, j) ;
	int local_j ;

	// Root process actually gets the value. //
	if (root_process == rank) {
		local_j = get_local_col_index(mat, j);
		value = mat->data[mat_localarray_index(mat, i, local_j)] ;
	}
	// Broadcasts value to other processes. //
	MPI_Bcast(&value, 1, MPI_DOUBLE, root_process, MPI_COMM_WORLD) ;

	return value ;
}		/* -----  end of function get_matrix_entry  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  gauss_lu
 *    Arguments:  Matrix * mat - Matrix to decompose.
 *                Matrix * lower - Lower triangular matrix in LU decomp.
 *                Matrix * upper - Upper triangular matrix in LU decomp.
 *                int * row_locations - Vector representing permuation matrix.
 *  Description:  Performs LU decomposition on the matrix mat such that PA = LU.
 * =====================================================================================
 */

void gauss_lu(Matrix * mat, Matrix * lower, Matrix * upper, int * permute_mat) {
	double pivot, pivot_factor ;
	int i,j,k ;
	int dimension = mat->dimension ;
	int block_dimension = mat->block_dimension ;
	int * row_locations = malloc(sizeof(int)*dimension) ;

	// Initialise row locations and permutation vector. //
	for (i = 0 ; i < mat->dimension ; ++i) {
		row_locations[i] = i ;
		permute_mat[i] = i ;
	}

	// Copy the matrix to upper. //
	memcpy(upper->data, mat->data, sizeof(double)*dimension*block_dimension) ;

	// Choose Pivot and divide across by pivot factor. //
	for (i = 0 ; i < dimension ; ++i) {
		// Swap rows such that the row with the largest pivot is considered. //
		partial_pivot(lower, upper, row_locations, permute_mat, i) ;
		// Accesess done using permutation vector index rather than raw indices. ///
		pivot = get_matrix_entry(upper,row_locations[i],i) ;
		set_matrix_entry(lower, row_locations[i], i, 1) ;
		for (j = i+1 ; j < dimension ; ++j) {
			// Generate pivot factor by pulling pivot from other processes. //
			pivot_factor = get_matrix_entry(upper,row_locations[j],i) / pivot ;
			// Only work on the remaining columns. //
			for (k = 0 ; k < block_dimension ; ++k) {
				upper->data[k+block_dimension*row_locations[j]] -= pivot_factor * upper->data[k+block_dimension*row_locations[i]] ;
			}
			if (get_col_host(upper, i) == rank) {
				lower->data[i/nproc+block_dimension*row_locations[j]] = pivot_factor ;
			}
		}
	}

	// Reconstruct permutated L and U using the permutation vector. //
	double * temp_l_data = malloc(sizeof(double)*dimension*block_dimension) ;
	double * temp_u_data = malloc(sizeof(double)*dimension*block_dimension) ;
	for (i = 0 ; i < dimension ; ++i) {
		memcpy(&temp_l_data[mat_localarray_index(lower,i,0)], 
				&lower->data[mat_localarray_index(lower,row_locations[i],0)], sizeof(double)*lower->block_dimension) ;
		memcpy(&temp_u_data[mat_localarray_index(upper,i,0)], 
				&upper->data[mat_localarray_index(upper,row_locations[i],0)], sizeof(double)*upper->block_dimension) ;
	}


	// Switch arrays to permutated array. //
	free(row_locations) ;
	free(upper->data) ;
	free(lower->data) ;
	upper->data = temp_u_data ;
	lower->data = temp_l_data ;
}		/* -----  end of function matrix  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  print_matrix
 *    Arguments:  Matrix * mat - Matrix to print.
 *  Description:  Prints the matrix.
 * =====================================================================================
 */

void print_matrix(Matrix * mat) {
	int i,j,k ;
	double val ;
	if (rank==0) printf("-----\n") ;
  	for (i=0;i<mat->dimension;i++) {
    	for (j=0;j<mat->dimension;j++) {
      		val = get_matrix_entry(mat,i,j);
      		if (rank==0) printf("%lf ", val);
		}
    	if (rank==0) printf("\n");
    }
}		/* -----  end of function print_matrix  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  invert
 *    Arguments:  Matrix * mat - Matrix to be inverted.
 *      Returns:  Pointer to the inverted matrix.
 *  Description:  Inverts the matrix by carrying out LU decomposition and solving.
 *                Matrix has to be unpermuted in order to get the inverse of A. The
 *                chain of operation is as follows.
 *                (1) Decompose LU = PA
 *                (2) Find inverse M of LU, (PA)^(-1) = M
 *                (3) Apply M to P to generate A^(-1), MP = A^(-1)
 * =====================================================================================
 */

Matrix * invert(Matrix * mat) {
	// ..............................INTIALISING......................................//
	
	int i,j ;
	// Allocate memory for the lower and upper matrices in LU decomposition and inverse./
	Matrix * lower = alloc_matrix(mat->dimension) ;	
	Matrix * upper = alloc_matrix(mat->dimension) ;	
	Matrix * invert_mat = alloc_matrix(mat->dimension) ;
	// Distributed b, alpha and ans vector in systems L*(alpha) = b and U*ans = alpha. //
	double * b_vec = malloc(mat->block_dimension*sizeof(double)) ;
	double * alpha_vec = malloc(mat->block_dimension*sizeof(double)) ;
	double * ans_vec = malloc(mat->block_dimension*sizeof(double)) ;
	// Buffer to store columns before writing to inverse matrix. //
	double * col_buffer = malloc(sizeof(double)*mat->dimension) ;
	// Array storing the size of blocks on each process. //
	int * offsets = malloc(sizeof(int)*nproc) ;
	int * blocks_per_process = malloc(sizeof(int)*nproc) ;
	MPI_Allgather(&mat->block_dimension, 1, MPI_INT,
                  blocks_per_process, 1, MPI_INT,
                  MPI_COMM_WORLD) ;
	// Generate the offsets. This is needed for MPI_ALLGATHERV. //
	offsets[0] = 0 ;
	for (i = 1 ; i < nproc ; ++i) {
		offsets[i] = offsets[i-1] + blocks_per_process[i-1] ;
	}
	// Indices map which stores the global column number for each local column. //
	int * global_col_indices = malloc(sizeof(int)*lower->block_dimension) ;
	for (i = 0 ; i < lower->block_dimension; ++i) {
		global_col_indices[i] = rank + nproc*i ;
	}

	// ...............................DECOMPOSE ........................................//
	
	// Use a permutation vector to record pivot swaps rather than swapping data locations. //
	int * permute_mat = malloc(mat->dimension*sizeof(int)) ;
	// Break mat into L and U matrix such that LU = PA. P is permutation matrix. Uses partial pivotting. //
	gauss_lu(mat, lower, upper, permute_mat) ;

	// ..............................FIND INVERSE.......................................//

	// Get the inverse by solving linear system L*(alpha) = b and then U*ans = alpha. //
	// Have to solve this systems for each b which is a column of the identity. //
	for (i = 0 ; i < mat->dimension ; ++i) {
		// Initialise vectors to zero. //
		int root_process = get_col_host(mat, i) ;
		memset(b_vec, 0, mat->block_dimension*sizeof(double)) ;
		memset(ans_vec, 0, mat->block_dimension*sizeof(double)) ;
		memset(alpha_vec, 0, mat->block_dimension*sizeof(double)) ;
		// Set up the corresponding identity matrix. //
		if (rank == root_process) {
			int local_col_index = get_local_col_index(mat, i) ;
			b_vec[local_col_index] = 1 ;
		}

		// Solve L*(\alpha) = b where b is an identity column. //
		forward_sub_inv_solve(lower, b_vec, alpha_vec, global_col_indices, i) ;
		// Solve U*m = \alpha to generate inverse column. //
		backward_sub(upper, alpha_vec, ans_vec, global_col_indices) ;

		// Gather up column data from each process and combine to form a column of the inverse. //
		MPI_Gatherv(ans_vec, mat->block_dimension, MPI_DOUBLE, col_buffer, blocks_per_process, offsets, 
				MPI_DOUBLE, root_process, MPI_COMM_WORLD) ;
		
		if (rank == get_col_host(mat, i)) {
			int local_j = get_local_col_index(mat, i) ;
			for (j = 0 ; j < mat->dimension ; ++j) {
				int host = get_col_host(mat, j) ;
				int local_j_host = (j-host) / nproc ;
				invert_mat->data[mat_localarray_index(invert_mat, j, local_j)] = col_buffer[offsets[host]+local_j_host] ;
			}
		}
	}

	//.............................APPLY TO PERMUTATION MATRIX.........................//
	
	MPI_Status stat ;
	// Apply M^{-1} to P in order to obatin A^{-1} as M = (PA)^{-1}. //
	double * new_columns = malloc(sizeof(double)*mat->block_dimension*mat->dimension) ;
	// Have to swap columns between processes when applying permutation matrix. //
	double * buffer = malloc(sizeof(double)*mat->dimension) ;
	for (i = 0 ; i < mat->dimension ; ++i) {
		int col_index_required = permute_mat[i] ;
		int source = get_col_host(mat, col_index_required) ;
		int dest = get_col_host(mat, i) ;
		// Source and destination on same core. //
		if (rank == source && rank == dest) {
			int local_j_dest = get_local_col_index(mat, i) ;
			int local_j_source = get_local_col_index(mat, permute_mat[i])  ;
			for (j = 0 ; j < mat->dimension ; ++j) {
				new_columns[local_j_dest*mat->dimension+j] = invert_mat->data[mat_localarray_index(mat,j,local_j_source)] ;
			}
		} 
		// Source and destination on different cores. //
		else if (rank == source) {
			int local_j = get_local_col_index(mat, permute_mat[i]) ;
			for (j = 0 ; j < mat->dimension ; ++j) {
				buffer[j] = invert_mat->data[mat_localarray_index(mat,j,local_j)] ;
			}
			MPI_Send(buffer, mat->dimension, MPI_DOUBLE, dest, 1, MPI_COMM_WORLD) ;
		} else if (rank == dest) {
			int local_j = get_local_col_index(mat, i) ;
			MPI_Recv(&new_columns[local_j*mat->dimension], mat->dimension, MPI_DOUBLE, source, 1, MPI_COMM_WORLD, &stat) ;
		}
	}

	// Overwrite columns of the permuted inverse with the unpermuted inverse on each local process. //
	for (i = 0 ; i < mat->block_dimension ; ++i) {
		for (j = 0 ; j < mat->dimension ; ++j) {
			invert_mat->data[mat_localarray_index(mat,j,i)] = new_columns[i*mat->dimension+j] ;
		}
	}

	// Free the heap. //
	free(b_vec) ;
	free(ans_vec) ;
	free(alpha_vec) ;
	free(lower) ;
	free(upper) ;
	free(buffer) ;
	free(new_columns) ;
	free(col_buffer) ;
	free(permute_mat) ;
	free(global_col_indices) ;

	return invert_mat ;
}		/* -----  end of function invert  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  forward_sub_inv_solve
 *    Arguments:  Matrix * lower - Lower triangular matrix L in system L*(ans) = b.
 *                double * vec - Column of identity matrix in system L*(ans) = b.
 *                double * ans - Vector onto which the solution of the system is written.
 *                int * global_col_indices - Lookup table mapping local column indices to
 *                global column indices.
 *                int ident_col_num - The row index of the non zero entry in b.
 *  Description:  Solves a lower triangular system L*(ans) = b for when b is a column of
 *                the identity matrix with exactly one non-zero entry located at
 *                ident_col_num.
 * =====================================================================================
 */

void forward_sub_inv_solve(Matrix * lower, double * vec, double * ans, int * global_col_indices,
		int ident_col_num) {
	
	int i,j ;
	// Start at ident_col_num as all preceding terms are zero. Forward substitution. //
	for (i = ident_col_num ; i < lower->dimension ; ++i) {
		double sum_of_left_terms = 0 ;
		double tot_left_side = 0 ;
		int root_process = get_col_host(lower, i) ;

		// Add up terms to the left of the current row index multiplied by their corresponding
		// terms in the answer vector. //
		j = 0 ;
		while (global_col_indices[j] < i) {
			sum_of_left_terms += ans[j]*lower->data[mat_localarray_index(lower,i,j)] ;
			++j ;
		}

		// Add up all these terms and send to the process in charge of this column i. //
		MPI_Reduce(&sum_of_left_terms, &tot_left_side, 1, MPI_DOUBLE, MPI_SUM, root_process,
				MPI_COMM_WORLD) ;
		
		if (rank == root_process) {
			int local_col_index = get_local_col_index(lower, i) ;
			ans[local_col_index] = (vec[local_col_index]-tot_left_side)
				/(lower->data[mat_localarray_index(lower,i,local_col_index)]) ;
		}
	}
}		/* -----  end of function forward_sub  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  back_sub
 *    Arguments:  Matrix * upper - Lower triangular matrix U in system U*(ans) = b.
 *                double * vec - Column of identity matrix in system U*(ans) = b.
 *                double * ans - Vector onto which the solution of the system is written.
 *                int * global_col_indices - Lookup table mapping local column indices to
 *                global column indices.
 *  Description:  Uses back substitution to solve the linear system U*(ans) = b.
 * =====================================================================================
 */

void backward_sub(Matrix * upper, double * vec, double * ans, int * global_col_indices) {
	int i,j ;
	// Work backwards from the end of the matrix to the top. //
	for (i = upper->dimension-1 ; i >= 0 ; --i) {
		double sum_of_left_terms = 0 ;
		double tot_left_side = 0 ;
		int root_process = get_col_host(upper, i) ;

		// Add up terms to the right of the current row index multiplied by their corresponding
		// terms in the answer vector. //
		j = upper->block_dimension-1 ;
		while (global_col_indices[j] > i) {
			sum_of_left_terms += ans[j]*upper->data[mat_localarray_index(upper,i,j)] ;
			--j ;
		}

		// Add up all these terms and send to the process in charge of this column i. //
		MPI_Reduce(&sum_of_left_terms, &tot_left_side, 1, MPI_DOUBLE, MPI_SUM, root_process, 
				MPI_COMM_WORLD) ;

		if (rank == root_process) {
			int local_col_index = get_local_col_index(upper, i) ;
			ans[local_col_index] = (vec[local_col_index]-tot_left_side)
				/upper->data[mat_localarray_index(upper, i, local_col_index)] ; 
		}
	}
}		/* -----  end of function forward_sub  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  partial_pivot
 *    Arguments:  Matrix * lower - Lower triangular matrix L in LU decomposition.
 *                Matrix * upper - Upper triangular matrix U in LU decomposition.
 *                int * row_locations - Vector storing actual row locations in memory -
 *                  this saves actually swapping the rows which is costly.
 *                int * permute_mat - Vector that represents successive applications -
 *                  of the permutation matrix P_0 * P_1 .... P_N
 *                int diag_num - The index of the diagonal being operated on in the LU
 *                  decomposition.
 *  Description:  For a given row index find the column entry below the diagonal with
 *                the largest magnitude. Apply this permutation to the permutation vector
 *                and update the row_locations vector to record this swap without actually
 *                swapping the data.
 * =====================================================================================
 */

void partial_pivot(Matrix * lower, Matrix * upper, int * row_locations, int * permute_mat, int diag_num) {
	int i ;
	int index = diag_num ;
	int root_process = get_col_host(upper, diag_num) ;

	// Find the entry below the diagonal with the largest magnitude. //
	if (rank == root_process) {
		int local_j = get_local_col_index(upper, diag_num);
		double largest_pivot = upper->data[mat_localarray_index(upper, row_locations[diag_num], local_j)] ;
		for (i = diag_num+1 ; i < upper->dimension ; ++i) {
			double trial_pivot = upper->data[mat_localarray_index(upper, row_locations[i], local_j)] ;
			if (fabs(trial_pivot) > fabs(largest_pivot)) {
				largest_pivot = trial_pivot ;
				index = i ;
			}
		}
	}

	MPI_Bcast(&index, 1, MPI_INT, root_process, MPI_COMM_WORLD) ;

	if (index != diag_num) {
		// Update the actual row locations. //
		int prev_index = row_locations[diag_num] ;
		row_locations[diag_num] = row_locations[index];
		row_locations[index] = prev_index ;
		// Update the true permutation matrix such that P_1*P_2.....P. /
		int prev_perm = permute_mat[prev_index] ;
		permute_mat[prev_index] = permute_mat[row_locations[diag_num]] ;
		permute_mat[row_locations[diag_num]] = prev_perm ;
	}
}		/* -----  end);f function pivot  ----- */
