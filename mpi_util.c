
/*
 * =====================================================================================
 *
 *       Filename:  mpi_utils.c
 *
 *    Description:  Source file for mpi_utils code.
 *
 *        Version:  1.0
 *        Created:  08/02/16 20:52:41
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Michael Tierney (MT), tiernemi@tcd.ie
 *
 * =====================================================================================
 */

#include "mpi_util.h"
#include "mpi/mpi.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  init
 *    Arguments:  int * argc - Number of command line arguments.
 *                char ** argv[] - CommandLine arguments.
 *  Description:  Init function for MPI that also initialises rank and nproc
 * =====================================================================================
 */

void init(int * argc, char ** argv[]) {
	MPI_Init(argc, argv) ;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank) ;
	MPI_Comm_size(MPI_COMM_WORLD, &nproc) ;
}
