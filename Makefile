CC=mpicc -O2
CFLAGS=-Wall

invert: invert.c matrix.o mpi_util.o
	${CC} -o invert invert.c matrix.o mpi_util.o ${CFLAG} 

matrix.o: matrix.c matrix.h
	${CC} -c matrix.c ${CFLAG} 

mpi_util.o: mpi_util.c mpi_util.h
	${CC} -c mpi_util.c ${CFLAG} 

clean: 
	rm -f matrix.o mpi_util.o invert

