#ifndef MATRIX_H_EPQ0Y3IJ
#define MATRIX_H_EPQ0Y3IJ

/* 
 * ===  STRUCT  ========================================================================
 *         Name:  Matrix
 *       Fields:  double * data - Array containing the block data for this process.
 *                int dimension - The dimension n of the full nxn Matrix.
 *                int block_dimension - The number of columns in the block on each process.
 *  Description:  Matrix whose data has been decomposed column cyclic among each process.
 * =====================================================================================
 */

typedef struct Matrix {
	double * data ;
	int dimension ;
	int block_dimension ;
	int rank ;
	int nproc ;
} Matrix ;		/* -----  end of struct Matrix  ----- */

Matrix * alloc_matrix(int dimension) ;
void free_matrix(Matrix * mat) ;
void set_matrix_entry(Matrix * mat, int i, int j, double value) ;
double get_matrix_entry(Matrix * mat, int i, int j) ;
void gauss_lu(Matrix * mat, Matrix * lower, Matrix * upper, int * permute_mat) ;
void print_matrix(Matrix * mat) ;
Matrix * invert(Matrix * mat) ;
void forward_sub_inv_solve(Matrix * lower, double * vec, double * ans, int * global_col_indices,  int ident_col_num) ;
void backward_sub(Matrix * upper, double * vec, double * ans, int * global_col_indices) ;
void partial_pivot(Matrix * lower, Matrix * upper, int * row_locations, int * permute_mat, int diag_num) ;

#endif /* end of include guard: MATRIX_H_EPQ0Y3IJ */


